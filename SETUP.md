# Setting up a React app with Tailwindcss

1. Install TW, postcss and watch into the project using the VSCode terminal:

    npm i tailwindcss postcss-cli autoprefixer@latest
    npm i -D postcss postcss-cli
    npx tailwindcss init --full
    npm install watch

2. Create the following files in "./src/styles":

    main.css
    tailwind.css

3. Import the following into "tailwind.css":

    @tailwind base;
    @tailwind components;
    @tailwind utilities;

4. Create a file called "postcss.config.js" and paste the following inside:

    const tailwinds = require("tailwindcss");

    module.exports = {
        plugins: [tailwinds("./tailwind.config.js"), require("autoprefixer")],
    }

5. In "package.json", add the following scripts:

    "build:css": "postcss src/styles/tailwind.css -o src/styles/main.css",
    "build:watch": "postcss src/styles/tailwind.css -o src/styles/main.css --watch"

    Then run the following command:

    npm run build:css

6. In "index.js", change the import from:

    import './index.css'
    to
    import './styles/main.css'

7. Test it by changing the main "App.js" component and running the build script